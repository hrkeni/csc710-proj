with Ada.Text_IO;                       use Ada.Text_IO;
with Ada.Integer_Text_IO;           use Ada.Integer_Text_IO;
package body CriticalPath is
    NC : Integer := 1;
    NE : Integer := 1;

    procedure Create_Graph is
    begin
        Add_Edge('A', 'B', 10);
        Add_Edge('A', 'C', 10);
        Add_Edge('B', 'D', 5);
        Add_Edge('C', 'D', 2);
        Add_Edge('D', 'E', 3);
        Add_Edge('E', 'F', 5);
        Add_Edge('E', 'G', 5);
        Add_Edge('E', 'H', 5);
        Add_Edge('G', 'I', 3);
        Add_Edge('F', 'J', 4);
        Add_Edge('F', 'K', 4);
        Add_Edge('I', 'J', 4);
        Add_Edge('I', 'K', 4);
        Add_Edge('H', 'J', 6);
        Add_Edge('H', 'K', 6);
        Add_Edge('J', 'M', 7);
        Add_Edge('K', 'L', 4);
        Add_Edge('M', 'P', 7);
        Add_Edge('L', 'P', 3);
        Add_Edge('P', '*', 2);
    end;

   procedure Add_Edge
    (From, To : in Character; Weight : in Integer) is
   begin
       Edges(NE).From := From;
       Edges(NE).To := To;
       Edges(NE).Weight := Weight;
       NE := NE + 1;

   end;

   procedure Print_Graph is

   begin
       for i in 1..NC loop
           put_line("From: ");
           put_line(Edge.From);
           put_line(" To: ");
           put_line(Edge.To);
           put_line(" Weight: ");
           put_line(Weight);

       end loop;
   end;

   procedure Clear (Graph : in out Graph_Type) is
   begin
     Graph.Number := 0;
    end Clear;

    function Index_Of (Graph : Graph_Type;
                       Vertex: Vertex_Type) return Positive is
    begin
       for I in Graph.Vertices'First..Graph.Number loop
          if Graph.Vertices(I).Info=Vertex then
             return I;
          end if;
       end loop;
       return Graph.Number+1;
    end Index_Of;

    procedure Add_Vertex (Graph  : in out Graph_Type;
                          Vertex : in     Vertex_Type) is
       Index: Positive := Index_Of (Graph, Vertex);
    begin
       if Index <= Graph.Number then
          raise Vertex_Error;
       elsif Index > Graph.Max_Vertices then
          raise Overflow;
       else
          Graph.Vertices (Index) := (Vertex, False);
          Graph.Number := Index;

          for Destination in Graph.Vertices'Range loop
             Graph.Edges(Index,Destination).Defined := False;
          end loop;
       end if;
end Add_Vertex;

    procedure Add_Edge (Graph: in out Graph_Type;
                        Head, Tail: Vertex_Type; Weight: Edge_Weight) is
       H: Positive := Index_Of (Graph, Head);
       T: Positive := Index_Of (Graph, Tail);
    begin
       if H>Graph.Number or T>Graph.Number then
          raise Edge_Error;
       else
          Graph.Edges (H,T) := (Weight, True);
       end if;
    end Add_Edge;

    function Weight_Of (Graph: Graph_Type;
                        Head, Tail: Vertex_Type) return Edge_Weight is
       H: Positive := Index_Of (Graph, Head);
       T: Positive := Index_Of (Graph, Tail);
    begin
       if H>Graph.Number or T>Graph.Number then
          raise Edge_Error;
       else
          return Graph.Edges(H,T).Weight;
       end if;
    end Weight_Of;

    function Successors (Graph: Graph_Type;
                         Vertex: Vertex_Type) return Vertex_List is
       Index: Positive := Index_Of (Graph, Vertex);
       List: Vertex_List (1..Graph.Number);  
       N: Natural := 0;
    begin
       for Destination in Graph.Vertices'First..Graph.Number loop
          if Graph.Edges (Index, Destination).Defined then
         N := N+1;
         List(N) := Graph.Vertices(Destination).Info;
          end if;
       end loop;
       return List(1..N);
    end Successors;

    procedure Clear_All_Marks (Graph: in out Graph_Type) is
    begin
       for I in Graph.Vertices'First..Graph.Number loop
          Graph.Vertices(I).Marked := False;
       end loop;
    end Clear_All_Marks;

    function Marked (Graph: Graph_Type;
                     Vertex: Vertex_Type) return Boolean is
       Index: Positive := Index_Of (Graph, Vertex);
    begin
       if Index > Graph.Number then
          raise Vertex_Error;
       else
          return (Graph.Vertices(Index).Marked);
       end if;
    end Marked;

    procedure Mark_Vertex (Graph : in out Graph_Type;
                           Vertex: in     Vertex_Type) is
       Index: Positive := Index_Of (Graph, Vertex);
    begin
       if Index > Graph.Number then
          raise Vertex_Error;
       else
          Graph.Vertices(Index).Marked := True;
       end if;
    end Mark_Vertex;
end CriticalPath;
