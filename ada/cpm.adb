with Ada.Text_IO;
with Ada.Text_IO.Unbounded_IO;
with Ada.Strings.Unbounded;

package body CPM is
    package IO renames Ada.Text_IO;
    package SUIO renames Ada.Text_IO.Unbounded_IO;
    
    procedure main is
    begin
        Ada.Text_IO.Put_Line ("Hi there!");
    end main;
end CPM;
