with Ada.Containers.Vectors;
with Ada.Containers.Doubly_Linked_Lists;
use Ada.Containers;
package CriticalPath is
   type Edge is record
      To, From : Character;
      Weight   : Integer;
   end record;
   Type Edges is array (1..30) of Edge;
   Type Nodes is array (1..30) of Character;

   procedure Add_Edge
   (From, To : in Character; Weight : in Integer);

   procedure Print_Graph;
   procedure Create_Graph;

end CriticalPath;
