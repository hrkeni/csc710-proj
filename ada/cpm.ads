with Ada.Text_IO;
with Ada.Text_IO.Unbounded_IO;
with Ada.Strings.Unbounded;
with Ada.Containers.Vectors;

package CPM is

    type Edge is
        record
            From_Node : Character;
            To_Node : Character;
            Weight : Integer;
        end record;
    type Edges_Vector is new Ada.Containers.Vectors
        (Element_Type => Edge, Index_Type => Integer);
    type Nodes_Vector is new Ada.Containers.Vectors
        (Element_Type => Character, Index_Type => Integer);
    type Graph is
        record
            Edges : Edges_Vector;
            Nodes : Nodes_Vector;
        end record;

    procedure main;

    private
end CPM;
