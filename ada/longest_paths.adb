

package body Longest_Paths is
   function Longest_Path
     (G : Graphs.Vector; Source : Node; Target : Node) return Paths.List
   is
      use Adjacency_Lists, Node_Maps, Paths, Graphs;
      Reached  : array (Node) of Boolean := (others => False);
      Reached_From : array (Node) of Node;
      So_Far   : array (Node) of Distance := (others => Distance'Last);
      The_Path : Paths.List := Paths.Empty_List;
      Nearest_Distance : Distance;
      Next     : Node;
   begin
      So_Far(Source)  := 0.0;
      while not Reached(Target) loop
         Nearest_Distance := Distance'Last;
         Next := Source;
         for N in Node'First .. Node'Last loop
            if not Reached(N)
              and then So_Far(N) < Nearest_Distance then
                 Next := N;
                 Nearest_Distance := So_Far(N);
            end if;
         end loop;
         if Nearest_Distance = Distance'Last then
            return Paths.Empty_List;
         else
            Reached(Next) := True;
         end if;
         for E of G (Next) loop
            if not Reached(E.To) then
               Nearest_Distance := E.Length + So_Far(Next);
               if Nearest_Distance < So_Far(E.To) then
                  Reached_From(E.To) := Next;
                  So_Far(E.To) := Nearest_Distance;
               end if;
            end if;
         end loop;
      end loop;
      declare
         N : Node := Target;
      begin
         while N /= Source loop
            N := Reached_From(N);
            Prepend (The_Path, N);
         end loop;
      end;
      return The_Path;
   end;

   procedure Create_Graph is
   begin
       Add_Edge('A', 'B', 10);
       Add_Edge('A', 'C', 10);
       Add_Edge('B', 'D', 5);
       Add_Edge('C', 'D', 2);
       Add_Edge('D', 'E', 3);
       Add_Edge('E', 'F', 5);
       Add_Edge('E', 'G', 5);
       Add_Edge('E', 'H', 5);
       Add_Edge('G', 'I', 3);
       Add_Edge('F', 'J', 4);
       Add_Edge('F', 'K', 4);
       Add_Edge('I', 'J', 4);
       Add_Edge('I', 'K', 4);
       Add_Edge('H', 'J', 6);
       Add_Edge('H', 'K', 6);
       Add_Edge('J', 'M', 7);
       Add_Edge('K', 'L', 4);
       Add_Edge('M', 'P', 7);
       Add_Edge('L', 'P', 3);
       Add_Edge('P', '*', 2);
   end;

  procedure Add_Edge
   (From, To : in Character; Weight : in Integer) is
  begin
      Edges(NE).From := From;
      Edges(NE).To := To;
      Edges(NE).Weight := Weight;
      NE := NE + 1;

  end;

  procedure Print_Graph is

  begin
      for i in 1..NC loop
          put_line("From: ");
          put_line(Edge.From);
          put_line(" To: ");
          put_line(Edge.To);
          put_line(" Weight: ");
          put_line(Weight);

      end loop;
  end;
end Longest_Paths;
