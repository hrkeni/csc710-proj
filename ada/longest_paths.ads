with Ada.Containers.Vectors;
with Ada.Containers.Doubly_Linked_Lists;
use Ada.Containers;
generic
   type Node is range <>;
package Longest_Paths is
   type Distance is new Float range 0.0 .. Float'Last;
   type Edge is record
      To, From : Node;
      Length   : Distance;
   end record;
   package Node_Maps is new Vectors (Node, Node);
   package Adjacency_Lists is new Doubly_Linked_Lists (Edge);
   use Adjacency_Lists;
   package Graphs is new Vectors (Node, Adjacency_Lists.List);
   package Paths is new Doubly_Linked_Lists (Node);
   function Longest_Path
     (G : Graphs.Vector; Source : Node; Target : Node) return Paths.List
      with Pre => G (Source) /= Adjacency_Lists.Empty_List;
end Longest_Paths;
