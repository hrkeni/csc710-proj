/**
* This file contains the implementation for calculating the critical path
* and other related funtions for an activity graph
*/
#include "cpm.h"
#include <iostream>

graph myGraph;
map<char, int> longestPaths;

/**
 * Adds an edge to the graph
 * @param nodeFrom (char) Node from
 * @param nodeTo   (char) Node to
 * @param weight   (int) weight
 */
void addEdge(char nodeFrom, char nodeTo, int weight) {
    edge e;
    e.nodeFrom = nodeFrom;
    e.nodeTo = nodeTo;
    e.weight = weight;
    myGraph.edges.push_back(e);

    myGraph.nodes.insert(nodeFrom);
    myGraph.nodes.insert(nodeTo);
}

/**
 * Creates the graph. This graph is hardcoded.
 * '*' is the dummy end node
 */
void createGraph() {
    addEdge('A', 'B', 10);
    addEdge('A', 'C', 10);
    addEdge('B', 'D', 5);
    addEdge('C', 'D', 2);
    addEdge('D', 'E', 3);
    addEdge('E', 'F', 5);
    addEdge('E', 'G', 5);
    addEdge('E', 'H', 5);
    addEdge('G', 'I', 3);
    addEdge('F', 'J', 4);
    addEdge('F', 'K', 4);
    addEdge('I', 'J', 4);
    addEdge('I', 'K', 4);
    addEdge('H', 'J', 6);
    addEdge('H', 'K', 6);
    addEdge('J', 'M', 7);
    addEdge('K', 'L', 4);
    addEdge('M', 'P', 7);
    addEdge('L', 'P', 3);
    addEdge('P', '*', 2);
}

/**
 * Prints the complete graph
 */
void printGraph() {
    for(vector<edge>::iterator it = myGraph.edges.begin();
            it != myGraph.edges.end(); it++) {
        edge e = *it;
        cout << "From: " << e.nodeFrom << " To: " << e.nodeTo << " Wt: "
            << e.weight << endl;
    }

    for(set<char>::iterator it = myGraph.nodes.begin();
            it != myGraph.nodes.end(); it++) {
        cout <<"Node: " << *it << endl;
    }
}

/**
 * Returns a list of outgoing edges
 * Retuns (vector<edge>)
 */
vector<edge> getOutgoingEdgesOfNode(char node) {
    vector<edge> result;
    for(vector<edge>::iterator it = myGraph.edges.begin();
            it != myGraph.edges.end(); it++) {
        edge e = *it;
        if (e.nodeFrom == node) {
            result.push_back(e);
        }
    }
    return result;
}

/**
 * Returns a list of incoming edges
 * Returns (vector<edge>)
 */
vector<edge> getIncomingEdgesOfNode(char node) {
    vector<edge> result;
    for(vector<edge>::iterator it = myGraph.edges.begin();
            it != myGraph.edges.end(); it++) {
        edge e = *it;
        if (e.nodeTo == node) {
            result.push_back(e);
        }
    }
    return result;
}

/**
 * Recursive function to get the longest path to end node from this node
 * @param node (char)
 */
void getLongestPathsOf(char node) {
    // cout << "calcLongestPathsFromNodeOf: " << node << endl;
    vector<edge> inEdges = getIncomingEdgesOfNode(node);
    int max = 0;
    for(vector<edge>::iterator it = inEdges.begin();
            it != inEdges.end(); it++) {
        edge e = *it;
        if ((longestPaths[e.nodeFrom] + e.weight) > max) {
            max = longestPaths[e.nodeFrom] + e.weight;
        }
    }
    longestPaths[node] = max;

    vector<edge> outEdges = getOutgoingEdgesOfNode(node);
    for(vector<edge>::iterator it = outEdges.begin();
            it != outEdges.end(); it++) {
        edge e = *it;
        getLongestPathsOf(e.nodeTo);
    }
}

/**
 * Calculates the longest paths to end node for all nodes
 * starting from this node
 * @param  source (char) source node
 * @return        (int) longest path
 */
int calcLongestPathsFromNode(char source) {
    int result = 0;
    for(set<char>::iterator it = myGraph.nodes.begin();
            it != myGraph.nodes.end(); it++) {
        if (source == *it) {
            longestPaths[*it] = 0;
        } else {
            longestPaths[*it] = -1;
        }
    }

    vector<edge> srcEdges = getOutgoingEdgesOfNode(source);

    for(vector<edge>::iterator it = srcEdges.begin();
            it != srcEdges.end(); it++) {
        edge e = *it;
        getLongestPathsOf(e.nodeTo);
    }

    return result;
}

/**
 * Prints all the information about each node
 */
void printLongestPaths() {
    for(set<char>::iterator it = myGraph.nodes.begin();
            it != myGraph.nodes.end(); it++) {
       char node = *it;
       cout << "Earliest start time of " << node << " is "
             << getEarliestStartTime(node)<<endl;
       cout << "Latest start time of " << node << " is "
             << getLatestStartTime(node) << endl;
       cout << "Slack time of " << node << " is "
             << getSlackTime(node) << endl;
       cout << node << " is a critical path activity: "
             << (isCriticalActivity(node) ? "true" : "false") << endl;
    }
}

/**
 * Get the earliest start time of a node
 * @param  node (char) node
 * @return      (int) the earliest start time
 */
int getEarliestStartTime(char node) {
    return longestPaths[node];
}

/**
 * Get the latest start time of a node
 * @param  node (char) node
 * @return      (int) the latest start time
 */
int getLatestStartTime(char node) {
    if(node=='*') {  // the latest start time of the last node will be
                     // the same as its earliest start time
    	return getEarliestStartTime(node);
    }
    vector<edge> outEdges = getOutgoingEdgesOfNode(node);
    vector<edge>::iterator it = outEdges.begin();
    edge e = *it;
    return longestPaths[e.nodeTo] - e.weight;
}

/**
 * Get the slack time of a node
 * @param  node (char) node
 * @return      (int) slack time
 */
int getSlackTime(char node)
{
	return getLatestStartTime(node) - getEarliestStartTime(node);
}

/**
 * Check if a node represents a critical activity
 * @param  node (char) node
 * @return      (bool) is critical activity
 */
bool isCriticalActivity(char node)
{
	return (getSlackTime(node)==0);
}
