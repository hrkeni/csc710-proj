#ifndef CPM_H
#define CPM_H

#include <vector>
#include <set>
#include <map>
using namespace std;

struct edge {
    char nodeFrom;
    char nodeTo;
    int weight;
};

struct graph {
    vector<edge> edges;
    set<char> nodes;
};

void createGraph();
void printGraph();
vector<edge> getOutgoingEdgesOfNode(char node);
vector<edge> getIncomingEdgesOfNode(char node);
int calcLongestPathsFromNode(char source);
void printLongestPaths();
int getEarliestStartTime(char node);
int getLatestStartTime(char node);
int getNodeSlackTime(char node);
bool isCriticalActivity(char node);
int getSlackTime(char node);

#endif /* end of include guard: CPM_H */
