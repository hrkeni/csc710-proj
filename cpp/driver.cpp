#include "cpm.h"
#include <iostream>

int main() {
    createGraph();
    calcLongestPathsFromNode('A');

    cout << "SPCT: " << getEarliestStartTime('*') << endl;
    cout <<"Earliest start times of each activity: " << endl;
    printLongestPaths();
}
